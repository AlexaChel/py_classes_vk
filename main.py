import requests
from pprint import pprint


TOKEN = '10af443eca9596ff0125192fc6f20368da3d30ddee080da4d1bbe1d9068932b9a3125cd0c56bf365b0e8c'


def print(user):
    pprint(f'https://vk.com/{user.get_info()[0]["domain"]}')


class User:
    def __init__(self, token='', id=0):
        self.token = token
        self.id = id
        pass

    def get_params(self):
        params = {
            'fields': 'nickname, domain, sex, bdate, city, country, timezone, photo_50, '
                      'has_mobile, contacts, online, relation, last_seen, status',
            'access_token': self.token,
            'v': '5.52'
        }

        if self.id:
            params['user_id'] = self.id

        return params

    def get_info(self):
        params = self.get_params()
        response = requests.get(
            'https://api.vk.com/method/users.get',
            params=params
        )
        return response.json()['response']

    def get_friends(self):
        params = self.get_params()
        friends = requests.get(
            'https://api.vk.com/method/friends.get',
            params=params
        )
        return friends.json()['response']['items']

    def get_general_friends(self, user):
        general_friends = []
        friends1 = self.get_friends()
        friends2 = user.get_friends()

        for friend1 in friends1:
            if friend1 in general_friends:
                continue

            for friend2 in friends2:
                if friend1['id'] == friend2['id']:
                    general_friends.append(User(TOKEN, friend1['id']))
                    break

        return general_friends


Alexa = User(TOKEN)
Denis = User(TOKEN, 421209593)
Alexa.get_general_friends(Denis)
print(Alexa)
print(Denis)



